$(document).ready(function() {

  //CARGAR MODULOS

  $("header").load("_header.html", function(){
    // MENÚ ACTIVO
    switch(true){
      case $('main').hasClass('page-menu'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='menu']").addClass('is-active'); break;
      case $('main').hasClass('page-dish'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='menu']").addClass('is-active'); break;
      case $('main').hasClass('page-life'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='green-life']").addClass('is-active'); break;
      case $('main').hasClass('page-locations'): $('header .nav a').removeClass('is-active'); $("header .nav a[data-section='locations']").addClass('is-active'); break;
    }
  });
  $("#mb-menu").load("_mobile.html");
  $("footer").load("_footer.html", function(){
    if( $(window).width() <= 576 ) $('.subscribe input').attr("placeholder","enter your e-mail");
  });

  //IMAGE BACKGROUNDS

  $(".img-bg, .stage").each(function(){
   $(this).css('background-image','url("' + $(this).data('img') +'")');
 });

  $('small').each(function(){ $(this).parent().addClass('sm'); });


});

// LOGO SCROLL

$(window).scroll(function() {
  if ( $(window).scrollTop() > 300 ){
    $('body').addClass('scrolled');
  } else {
    $('body').removeClass('scrolled');    
  }
});

// BOTON ORDENA TABLET

$('body').on('click', '#btnOrder, #btnOrderMobile, #modalOrder button', function() {
  if( $(window).width() <= 768 ){
    $('body').toggleClass('push');
  }
});

// BOTON MOVIL

$('body').on('click', '.mobile-btn', function() {
  $(".mobile-menu").toggleClass('open');
  $("body").toggleClass('body-no-scroll');
});

// BOTON DIET

if ( $('.btn.diet').length > 0 ){

  $('body').on('click', '.btn.diet', function() {

    $(this).toggleClass('selected');

    dietary = [];
    $('.btn.diet').each(function(){

      if ( $(this).hasClass('selected') ) dietary.push($(this).data('diet'));

    });

    $('.dish').each(function(){

      if( $(this).data('diet') != undefined && dietary.includes($(this).data('diet')) )
        $(this).addClass('show-tag');
      else
        $(this).removeClass('show-tag');

    });


  });

}

//SLIDER HOME

if ($('.home-swiper').length > 0) {


  var slides = $('.swiper-slide').length;

  function updSwiperNumericPagination() {
    this.el.querySelector( '.swiper-counter' )
    .innerHTML = '<span class="count">'+ (this.realIndex + 1) +'</span> of <span class="total">'+ slides +'</span> / signature salads';
  }

  var type = 0.5, 
  radius = '50px', 
  start = 0, 
  $elements = $('.swiper-slide'),
  numberOfElements = (type === 1) ?  $elements.length : $elements.length - 1, 
  slice = -360 * type / numberOfElements;
  var rotate, rotateReverse;

  var mySwiper = new Swiper('.home-swiper .swiper-container', {
    loop: true,
    direction: 'horizontal',
    slidesPerView: 3,
    spaceBetween: 0,
    centeredSlides: true,
    effect: 'slide',
    keyboard: {
      enabled: true,
      onlyInViewport: true,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="pag-' + index + '-' + slides + ' ' + className + '"></span>';
      },
    },
    on: { 
      init:        updSwiperNumericPagination,
      slideChange: updSwiperNumericPagination
    },
    breakpoints: {
      1024: {
        slidesPerView: 1,
        centeredSlides: false
      }
    }
  })

}

//SLIDER GREEN LIFE

if ($('.life-swiper').length > 0) {

  var mySwiper = new Swiper('.life-swiper .swiper-container', {
    loop: false,
    direction: 'horizontal',
    slidesPerView: 'auto',
    spaceBetween: 0,
    effect: 'slide',
    keyboard: {
      enabled: true,
      onlyInViewport: true,
    }
  })

}

//SLIDER MENU MOBILE

if ($('.menu-swiper').length > 0) {

  var mySwiper = new Swiper('.menu-swiper .swiper-container', {
    loop: false,
    direction: 'horizontal',
    slidesPerView: 'auto',
    spaceBetween: 0,
    effect: 'slide',
    keyboard: {
      enabled: true,
      onlyInViewport: true,
    }
  })

}

//SLIDER FOOTERLINKS MOBILE

if ($('.ftlinks-swiper').length > 0) {

  var mySwiper = new Swiper('.ftlinks-swiper .swiper-container', {
    loop: false,
    direction: 'horizontal',
    slidesPerView: 'auto',
    spaceBetween: 0,
    effect: 'slide',
    keyboard: {
      enabled: true,
      onlyInViewport: true,
    }
  })

}

// LOADER

$(window).on('load', function() {
  $('main').css({
    opacity: 1
  });
});


// AGREGAR EFECTOS DE ON SCROLL

$(window).on('load', function() {
  $('.element').attr('data-aos', 'fade-in-up');
  AOS.init();
});