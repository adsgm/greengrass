// MAPS

// $('.loc').click(function(){
//   var html = $(this).html();
//   $('#modalLoc .content').html(html);
//   $('#modalLoc').modal('show');
// });

var mapOptionsCDMX = {
  zoom: 12,
  center: {lat: 19.359112, lng: -99.169768},
  scrollwheel: false,
  styles : [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
}

// CREATE CDMX MAP
var mapCDMX = new google.maps.Map(document.getElementById("mapCDMX"), mapOptionsCDMX);

// ADD CDMX LOCATIONS
addMarker({lat: 19.305454, lng: -99.202467}, "acora", "ciudad de méxico", mapCDMX);
addMarker({lat: 19.406154, lng: -99.171592}, "condesa", "ciudad de méxico", mapCDMX);
addMarker({lat: 19.359112, lng: -99.169768}, "coyoacan", "ciudad de méxico", mapCDMX);

var mapOptionsEDOMX = {
  zoom: 12,
  center: {lat: 19.424266, lng: -99.024063},
  scrollwheel: false,
  styles : [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
}


// CREATE EDOMEX MAP
var mapEDOMX = new google.maps.Map(document.getElementById("mapEDOMX"), mapOptionsEDOMX);

// ADD EDOMEX LOCATIONS
addMarker({lat: 19.424266, lng: -99.024063}, "cdjardin", "estado de méxico", mapEDOMX);


function addMarker(latlng, pin, edo, map){

  var image = { url: 'img/ico/pin.png' };

  // Create marker
  var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    icon: image,
    animation: google.maps.Animation.DROP
  });
  marker.addListener('click', function() { 
    var html = $('.loc[data-loc=' + pin + ']').html();
    $('#modalLoc .content').html(html);
    $('#modalLoc .h1').html(edo);
    $('#modalLoc').modal('show');
  });

}

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

function clearMarkers() {
  setMapOnAll(null);
}

function deleteMarkers() {
  clearMarkers();
  markers = [];
}